# Frontend

This project will build the frontend of cheshmanbazar using `Angular` framework.

## How to run / serve

In order to run the local server, run this command:

`docker-compose up --build`

Thank navigate to `localhost:4200` to see the development server. If everything works fine, you should see instant update by changing the `src/index.html` file.

### Some useful commands

- delete all containers and images: `docker system prune -a -f`
