import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { compoundName, entry } from './interfaces/app.info';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private urlName: string;
  listOfNames: string[];
  compoundNames: compoundName[];
  validFlag: BehaviorSubject<boolean> = new BehaviorSubject(true); // define a BehaviorSubject to detect the changes
  oneEntry = {} as entry;
  oneEntryCol = [
    'date',
    'time',
    'name',
    'wholeName',
    'id',
    'lastTransactionTime',
    'firstPurchase',
    'lastTransaction',
    'transaction',
    'transactionNumber',
    'transactionVolume',
    'transactionValue',
    'dayMinIndex',
    'dayMaxIndex',
    'yesterdayPrice',
    'EPS',
    'baseVolume',
    'maxPerimittedPrice',
    'minPermittedPrice',
    'sharesNumber',
    'persaleData',
    'legalData',
  ];
  private urlEntry: string;
  validChart: BehaviorSubject<boolean> = new BehaviorSubject(true); // define a BehaviorSubject to detect the changes
  private urlTreemap: string;

  constructor(private httpClient: HttpClient) {
    this.urlName = '/api/getNames';
    this.urlEntry = '/api/getOneEntry/';
    this.urlTreemap = '/api/getTreemapData';
    this.compoundNames = [];
  }

  public async getNames() {
    this.listOfNames = [];
    this.compoundNames = [];
    try {
      const response = await this.httpClient.get(this.urlName).toPromise();
      for (let index in response) {
        if (response[index].hasOwnProperty('name')) {
          this.listOfNames.push(response[index].name);
          if (response[index].hasOwnProperty('wholeName')) {
            this.compoundNames.push({
              id: response[index]._id,
              name: response[index].name,
              wholeName: response[index].wholeName,
            });
          }
        }
      }
      return this.listOfNames;
    } catch (error) {
      console.log('The Err is', error);
    }
  }

  public async findSpecificName(name: string) {
    if (this.compoundNames.length != 0) {
      const data = await this.filtering(name);
      return data;
    } else {
      await this.getNames();
      const data = await this.filtering(name);
      return data;
    }
  }

  private async filtering(name: string) {
    let data: compoundName[] = [];
    try {
      data = await this.compoundNames.filter((item) => {
        return item.name == name;
      });
      if (data.length != 0) {
        return data;
      } else {
        return [];
      }
    } catch (error) {
      console.log('The Err is', error);
    }
  }

  public async getOneInfo(id: number) {
    let url = this.urlEntry + id;
    try {
      const response = await this.httpClient.get(url).toPromise();
      for (let index in response) {
        Object.keys(response[index]).forEach((item, index) => {
          if (this.oneEntryCol.includes(item)) {
            this.oneEntry[item] = response[0][item];
          }
        });
        this.oneEntry.id = response[0]._id;
      }
      this.validChart.next(!this.validChart);
      console.log(this.oneEntry);
    } catch (error) {
      console.log('The Err is', error);
    }
    return this.oneEntry;
  }

  public async getTreemapData() {
    let response;
    try{
      response = await this.httpClient.get(this.urlTreemap).toPromise();
    }catch(err){
      console.log("the Err is",err)
    }
    return response;
  }
}
