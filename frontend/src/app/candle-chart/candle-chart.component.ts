import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DataService } from '../data.service';
import { ThemeOption } from 'ngx-echarts';
import { EChartsOption } from 'echarts';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-candle-chart',
  templateUrl: './candle-chart.component.html',
  styleUrls: ['./candle-chart.component.scss'],
})
export class CandleChartComponent implements OnInit, OnDestroy {
  chartOption: EChartsOption;
  candleData: number[][] = [];

  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;
  theme: string | ThemeOption = 'undefined';
  updateOptions: any;
  showFlag: boolean = true;

  @Input() itemId: number;
  private searchEventSubscription: Subscription;

  constructor(
    private dataService: DataService,
    private apiService: ApiService
  ) {
    this.searchEventSubscription = null;
  }

  ngOnInit(): void {
    this.renderId()
      .then(() => {
        this.setChartOptions(true);
      })
      .then(() => {
        this.searchEventSubscription = this.apiService.validChart.subscribe(
          () => {
            if (this.apiService.oneEntry != null) {
              if (
                this.apiService.oneEntry.firstPurchase[
                  this.apiService.oneEntry.firstPurchase.length - 1
                ] == 0
              ) {
                this.showFlag = false;
              } else {
                this.showFlag = true;
                this.setChartOptions(false);
              }
            }
          }
        );
      });

    this.dataService.themeStore.subscribe((val) => (this.theme = val));
    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeSubscription$ = this.resizeObservable$.subscribe((event) => {
      this.updateFont();
    });
  }

  async renderId() {
    if (!this.itemId) {
      await new Promise((resolve) => setTimeout(resolve, 1000));
      return this.renderId();
    }
  }

  private setChartOptions(state: boolean) {
    this.candleData = [];
    for (let index in this.apiService.oneEntry.firstPurchase) {
      this.candleData.push([
        this.apiService.oneEntry.firstPurchase[index],
        this.apiService.oneEntry.transaction[index],
        this.apiService.oneEntry.dayMinIndex[index],
        this.apiService.oneEntry.dayMaxIndex[index],
      ]);
    }
    if (state == true) {
      this.setOptions();
    } else {
      this.updateOption();
    }
  }

  setOptions() {
    this.chartOption = {
      title: {
        text: 'قیمت در طول روز',
        left: 'center',
        textStyle: {
          fontFamily: 'Vazir',
          fontSize: '17px',
        },
      },
      legend: {
        data: ['بازه روز', 'حجم'],
        top: '7%',
        left: 'center',
        textStyle: {
          fontFamily: 'Vazir',
          fontSize: '10px',
        },
      },
      tooltip: {
        show: true,
        trigger: 'axis',
        textStyle: {
          fontFamily: 'Vazir',
        },
      },
      toolbox: {
        show: true,
        feature: {
          dataZoom: {
            yAxisIndex: 'none',
          },
          dataView: { readOnly: false },
          restore: {},
          saveAsImage: {},
        },
      },
      dataZoom: [
        {
          type: 'inside',
          xAxisIndex: [0, 1],
        },
        {
          type: 'slider',
          xAxisIndex: [0, 1],
          height: 20,
          textStyle: {
            fontFamily: 'Vazir',
          },
        },
      ],
      xAxis: [
        {
          type: 'category',
          data: this.apiService.oneEntry.time,
          axisLabel: {
            fontSize: this.autoFontSize(120),
            fontFamily: 'Vazir',
          },
        },
        {
          type: 'category',
          data: this.apiService.oneEntry.time,
          gridIndex: 1,
          splitLine: { show: false },
          axisLabel: { show: false },
          axisTick: { show: false },
        },
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            fontSize: this.autoFontSize(120),
            fontFamily: 'Vazir',
          },
          min: 'dataMin',
          max: 'dataMax',
        },
        {
          type: 'value',
          scale: true,
          gridIndex: 1,
          axisLabel: { show: false },
          axisLine: { show: false },
          axisTick: { show: false },
          splitLine: { show: false },
        },
      ],
      series: [
        {
          name: 'بازه روز',
          type: 'candlestick',
          data: this.candleData,
          // alig: 'right',
          itemStyle: {
            color: '#006400',
            color0: '#8B0000',
            borderColor: undefined,
            borderColor0: undefined
          },
        },
        {
          name: 'حجم',
          type: 'bar',
          data: this.apiService.oneEntry.transactionVolume,
          xAxisIndex: 1,
          yAxisIndex: 1,
        },
      ],
      grid: [
        {
          // left: '10%',
          // right: '8%',
          height: '50%',
        },
        {
          // left: '10%',
          // right: '8%',
          top: '76%',
          height: '10%',
        },
      ],
      media: [
        {
          query: {
            maxWidth: 580,
          },
          option: {
            title: {
              textStyle: {
                fontSize: '17px',
              },
            },
            legend: {
              textStyle: {
                fontSize: '10px',
              },
            },
            toolbox: {
              show: false,
            },
          },
        },
        {
          query: {
            minWidth: 580,
          },
          option: {
            title: {
              textStyle: {
                fontSize: '17px',
              },
            },
            legend: {
              textStyle: {
                fontSize: '10px',
              },
            },
            toolbox: {
              show: true,
            },
          },
        },
      ],
    };
  }

  updateOption() {
    this.updateOptions = {
      xAxis: [
        {
          data: this.apiService.oneEntry.time,
        },
        {
          data: this.apiService.oneEntry.time,
        },
      ],
      series: [
        {
          data: this.candleData,
        },
        {
          data: this.apiService.oneEntry.transactionVolume,
        },
      ],
    };
  }

  updateFont() {
    this.updateOptions = {
      title: {
        textStyle: {
          fontSize: this.autoFontSize(55),
          // color: 'rgba(8, 102, 234, 1)',
        },
      },
      xAxis: {
        axisLabel: {
          fontSize: this.autoFontSize(120),
        },
      },
      yAxis: {
        axisLabel: {
          fontSize: this.autoFontSize(120),
        },
      },
      legend: {
        textStyle: {
          fontSize: this.autoFontSize(95),
        },
      },
    };
  }

  autoFontSize(num: number) {
    let width = document.getElementById('myChart').offsetWidth;
    let newFontSize = Math.round(width / num);
    return newFontSize;
  }

  ngOnDestroy(): void {
    this.searchEventSubscription.unsubscribe();
  }
}
