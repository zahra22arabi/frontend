import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DataService } from '../data.service';
import { ThemeOption } from 'ngx-echarts';
import { EChartsOption } from 'echarts';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { ApiService } from '../api.service';
import { Chalk } from 'src/themes/chalk';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent implements OnInit, OnDestroy {
  chartOption: EChartsOption;
  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;
  // theme: string | ThemeOption = Chalk;
  theme: string | ThemeOption = 'undefined';
  updateOptions: any;
  showFlag: boolean = true;

  @Input() itemId: number;
  private searchEventSubscription: Subscription;

  constructor(
    private dataService: DataService,
    private apiService: ApiService
  ) {
    this.searchEventSubscription = null;
  }

  ngOnInit(): void {
    this.renderId()
      .then(() => {
        this.setChartOptions(true);
      })
      .then(() => {
        this.searchEventSubscription = this.apiService.validChart.subscribe(
          () => {
            if (this.apiService.oneEntry != null) {
              if (
                this.apiService.oneEntry.firstPurchase[
                  this.apiService.oneEntry.firstPurchase.length - 1
                ] == 0
              ) {
                this.showFlag = false;
              } else {
                this.showFlag = true;
                this.setChartOptions(false);
              }
            }
          }
        );
      });

    this.dataService.themeStore.subscribe((val) => (this.theme = val));
    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeSubscription$ = this.resizeObservable$.subscribe((event) => {
      this.updateFont();
    });
  }

  async renderId() {
    if (!this.itemId) {
      await new Promise((resolve) => setTimeout(resolve, 1000));
      return this.renderId();
    }
    // console.log(this.itemId);
  }

  private setChartOptions(state: boolean) {
    if (state == true) {
      this.setOptions();
    } else {
      this.updateOption();
    }
  }

  setOptions() {
    this.chartOption = {
      title: {
        text: 'قیمت فروش',
        left: 'center',
        textStyle: {
          fontFamily: 'Vazir',
          fontSize: '17px',
        },
      },
      legend: {
        data: ['قیمت'],
        top: '7%',
        left: 'center',
        textStyle: {
          fontFamily: 'Vazir',
          fontSize: '10px',
        },
      },
      tooltip: {
        show: true,
        trigger: 'axis',
        textStyle: {
          fontFamily: 'Vazir',
        },
      },
      toolbox: {
        show: true,
        feature: {
          dataZoom: {
            yAxisIndex: 'none',
          },
          dataView: { readOnly: false },
          magicType: { type: ['line', 'bar'] },
          restore: {},
          saveAsImage: {},
        },
      },
      xAxis: {
        type: 'category',
        data: this.apiService.oneEntry.time,
        axisLabel: {
          fontSize: this.autoFontSize(120),
          fontFamily: 'Vazir',
        },
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          fontSize: this.autoFontSize(120),
          fontFamily: 'Vazir',
        },
      },
      series: {
        name: 'قیمت',
        type: 'bar',
        data: this.apiService.oneEntry.transaction,
      },
      dataZoom: [
        {
          type: 'inside',
        },
        {
          type: 'slider',
          textStyle: {
            fontFamily: 'Vazir',
          },
        },
      ],
      media: [
        {
          query: {
            maxWidth: 580,
          },
          option: {
            title: {
              textStyle: {
                fontSize: '17px',
              },
            },
            legend: {
              textStyle: {
                fontSize: '10px',
              },
            },
            toolbox: {
              show: false,
            },
          },
        },
        {
          query: {
            minWidth: 580,
          },
          option: {
            title: {
              textStyle: {
                fontSize: '17px',
              },
            },
            legend: {
              textStyle: {
                fontSize: '10px',
              },
            },
            toolbox: {
              show: true,
            },
          },
        },
      ],
    };
  }

  updateOption() {
    this.updateOptions = {
      xAxis: {
        data: this.apiService.oneEntry.time,
      },
      series: {
        data: this.apiService.oneEntry.transaction,
      },
    };
  }

  updateFont() {
    this.updateOptions = {
      title: {
        textStyle: {
          fontSize: this.autoFontSize(55),
          // color: 'rgba(8, 102, 234, 1)',
        },
      },
      xAxis: {
        axisLabel: {
          fontSize: this.autoFontSize(120),
        },
      },
      yAxis: {
        axisLabel: {
          fontSize: this.autoFontSize(120),
        },
      },
      legend: {
        textStyle: {
          fontSize: this.autoFontSize(95),
        },
      },
    };
  }

  autoFontSize(num: number) {
    let width = document.getElementById('myChart').offsetWidth;
    let newFontSize = Math.round(width / num);
    return newFontSize;
  }

  ngOnDestroy(): void {
    this.searchEventSubscription.unsubscribe();
  }
}
