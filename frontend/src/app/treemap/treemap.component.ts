import { Component, OnInit } from '@angular/core';
import { EChartsOption, number } from 'echarts';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { DataService } from '../data.service';
import { ThemeOption } from 'ngx-echarts';

@Component({
  selector: 'app-treemap',
  templateUrl: './treemap.component.html',
  styleUrls: ['./treemap.component.scss'],
})
export class TreemapComponent implements OnInit {
  chartOption: EChartsOption;
  updateOptions: any;
  retrievedData: any;
  private searchEventSubscription: Subscription;
  theme: string | ThemeOption = 'undefined';

  data: any = [];
  min: number;
  max: number;
  visualMin: number = -100;
  visualMax: number = 100;
  areaBase: string = 'volume';

  constructor(
    private dataService: DataService,
    private apiService: ApiService
  ) {
    this.searchEventSubscription = null;
  }

  ngOnInit(): void {
    this.getData().then(() => {
      this.setChartOptions(true);
    });
    this.searchEventSubscription = this.apiService.validFlag.subscribe(() => {
      if (this.data.length != 0) {
        console.log('in here');
        this.getData().then(() => {
          this.setChartOptions(false);
        });
      }
    });

    this.dataService.themeStore.subscribe((val) => {
      this.theme = val;
      this.fixColor(val);
    });
  }
  fixColor(color: string) {
    if (color == 'undefined') {
      document.documentElement.style.setProperty(
        '--light',
        'hsl(236, 89%, 63%)'
      );
      document.documentElement.style.setProperty(
        '--light-sel',
        'hsl(236, 77%, 83%)'
      );
    } else if (color == 'dark') {
      document.documentElement.style.setProperty(
        '--light',
        'hsl(337, 94%, 50%)'
      );
      document.documentElement.style.setProperty(
        '--light-sel',
        'hsl(337, 87%, 75%)'
      );
    }
  }

  async getData() {
    this.retrievedData = await this.apiService.getTreemapData();
    console.log(this.retrievedData);
  }

  setChartOptions(state: boolean) {
    this.organizeData(this.areaBase);
    if (state == true) {
      this.setOptions();
    } else {
      this.updateOption();
    }
  }

  organizeData(areaBase) {
    if (this.retrievedData.length == 0) {
      throw new Error("the data didn't fetched comepletely");
    }
    this.data = [];
    console.log(this.retrievedData);

    for (let i = 1; i < this.retrievedData.length; i++) {
      this.data.push({
        name: this.retrievedData[i].name,
        children: [],
      });

      for (let j = 0; j < this.retrievedData[i].children.length; j++) {
        let desiredvalue = [];
        if (areaBase == 'value') {
          desiredvalue = this.retrievedData[i].children[j].value.slice(1);
        } else if (areaBase == 'volume') {
          desiredvalue = [this.retrievedData[i].children[j].value[0]].concat(
            this.retrievedData[i].children[j].value.slice(2)
          );
        }
        let item = this.retrievedData[i].children[j].value[4];
        if (item > 0) {
          desiredvalue.push(
            number.linearMap(
              item,
              [0, this.retrievedData[i].max],
              [this.visualMax * 0.4, this.visualMax],
              true
            )
          );
        } else if (item < 0) {
          desiredvalue.push(
            number.linearMap(
              item,
              [this.retrievedData[i].min, 0],
              [this.visualMin, this.visualMin * 0.4],
              true
            )
          );
        } else {
          desiredvalue.push(0);
        }
        this.data[i - 1].children.push({
          name: this.retrievedData[i].children[j].name,
          wholeName: this.retrievedData[i].children[j].wholeName,
          value: desiredvalue,
          tooltipInfo: [
            this.formatNumber(desiredvalue[0]),
            this.formatNumber(desiredvalue[1]),
            desiredvalue[3].toFixed(2),
          ],
        });
      }
    }
    console.log(this.data);
  }

  setOptions() {
    this.chartOption = {
      title: {
        text: 'نقشه بورس تهران',
        left: 'center',
        textStyle: {
          fontFamily: 'Vazir',
        },
      },
      tooltip: {
        formatter: (info) => {
          console.log(info);
          let label;
          if (this.areaBase == 'value') {
            label = 'نسبت ارزش معاملات';
          } else if (this.areaBase == 'volume') {
            label = 'حجم معاملات';
          }
          let leg = `<div>${info.data.wholeName}
          <hr>
          ${label}: ${info.data.tooltipInfo[0]}
          <br/>
          قیمت نهایی: ${info.data.tooltipInfo[1]}
          <br/>
          درصد تغییرات: <span dir="ltr">${info.data.tooltipInfo[2]}%</span></div>`;
          return leg;
        },
        textStyle: {
          fontSize: '14',
          fontFamily: 'Vazir',
        },
      },
      series: [
        {
          name: 'نقشه بورس',
          top: 80,
          type: 'treemap',
          label: {
            show: true,
            formatter: '{b}',
            ellipsis: true,
            fontFamily: 'Vazir',
          },
          upperLabel: {
            show: true,
            fontFamily: 'Vazir',
            backgroundColor: 'transparent',
            color: 'white',
            position: 'insideRight',
          },
          itemStyle: {
            borderColor: 'black',
          },
          visualMin: this.visualMin,
          visualMax: this.visualMax,
          visualDimension: 4,
          levels: [
            {
              itemStyle: {
                borderWidth: 3,
                borderColor: '#000000',
                gapWidth: 3,
              },
              upperLabel: {
                show: false,
              },
            },
            {
              color: ['#942e38', '#aaa', '#269f3c'],
              colorMappingBy: 'value',
              itemStyle: {
                gapWidth: 1,
              },
            },
          ],
          data: this.data,
          breadcrumb: {
            top: '10%',
            emptyItemWidth: 10,
            itemStyle: {
              textStyle: {
                fontFamily: 'Vazir',
              },
            },
          },
        },
      ],
    };
  }

  updateOption() {
    this.updateOptions = {
      tooltip: {
        formatter: (info) => {
          console.log(info);
          let label;
          if (this.areaBase == 'value') {
            label = 'نسبت ارزش معاملات';
          } else if (this.areaBase == 'volume') {
            label = 'حجم معاملات';
          }
          let leg = `<div>${info.data.wholeName}
          <hr>
          ${label}: ${info.data.tooltipInfo[0]}
          <br/>
          قیمت نهایی: ${info.data.tooltipInfo[1]}
          <br/>
          درصد تغییرات: <span dir="ltr">${info.data.tooltipInfo[2]}%</span></div>`;
          return leg;
        },
      },
      series: [
        {
          data: this.data,
        },
      ],
    };
  }

  formatNumber(num: number) {
    let output: any;
    if (num > 1000000000) {
      output = `${(num / 1000000000).toFixed(3)} میلیارد`;
    } else if (num > 1000000) {
      output = `${(num / 1000000).toFixed(3)} میلیون`;
    } else if (num > 0) {
      output = num;
    } else if (num == 0) {
      output = 0;
    }
    return output;
  }

  changeView() {
    console.log(this.areaBase);
    if (this.data.length != 0) {
      this.setChartOptions(false);
    }
  }
}
