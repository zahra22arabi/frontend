import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  themeStore: BehaviorSubject<string> = new BehaviorSubject('undefined');
  selectedValue: string;
  constructor() {}
}
