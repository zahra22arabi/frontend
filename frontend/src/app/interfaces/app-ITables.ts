export interface IPurchase {
  purchase: any;
  transaction: number;
  sale: any;
}

export interface IPrice {
  firstPurchase: number;
  lastTransaction: number;
  yesterdayPrice: number;
}

export interface ITransaction {
  transactionNumber: number;
  transactionVolume: string;
  transactionValue: string;
  numOfShares: number;
}

export interface IStatus {
  time: string;
  status: string;
  market: any;
  industryGroup: any;
}

export interface IBase {
  baseVolume: string;
  marketValue: any;
  EPS: number;
  PE: any;
  PEGroup: any;
}

export interface IPersale {
  purchaseNumber: number;
  purchaseVolume: number;
  purchaseCost: number;
  saleCost: number;
  saleVolume: number;
  saleNumber: number;
}

export interface ILegal {
  legal: string,
  purchase: number;
  sale: number;
}

