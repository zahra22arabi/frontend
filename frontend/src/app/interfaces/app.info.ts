export interface compoundName {
  id: number;
  name: string;
  wholeName: string;
}

export interface entry {
  date: string;
  time: [string];
  name: string;
  wholeName: string;
  id: number;
  lastTransactionTime: string; //changed name
  firstPurchase: [number];
  lastTransaction: number;
  transaction: [number];
  transactionNumber: number;
  transactionVolume: [number];
  transactionValue: number;
  dayMinIndex: [number];
  dayMaxIndex: [number];
  yesterdayPrice: number;
  EPS: number;
  baseVolume: number;
  maxPerimittedPrice: number;
  minPermittedPrice: number;
  sharesNumber: number;
  persaleData: [[number]];
  legalData: [number];
}
