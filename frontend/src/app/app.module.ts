import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeSelectionComponent } from './theme-selection/theme-selection.component';
import { DataService } from './data.service';
import { InputComponent } from './input/input.component';
import { AppRoutingModule } from './app-routing.module';
import { NotfoundComponent } from './notfound/notfound.component';
import { InformationComponent } from './information/information.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { CandleChartComponent } from './candle-chart/candle-chart.component';
import { TreemapComponent } from './treemap/treemap.component';
import { SEOService } from './seo.service';

@NgModule({
  declarations: [
    AppComponent,
    ThemeSelectionComponent,
    InputComponent,
    NotfoundComponent,
    InformationComponent,
    BarChartComponent,
    CandleChartComponent,
    TreemapComponent,
  ],
  imports: [
    BrowserModule,
    NgxEchartsModule.forRoot({
      /**
       * This will import all modules from echarts.
       * If you only need custom modules,
       * please refer to [Custom Build] section.
       */
      echarts: () => import('echarts'), // or import('./path-to-my-custom-echarts')
    }),
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [DataService, SEOService],
  bootstrap: [AppComponent],
})
export class AppModule {}
