import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InputComponent } from './input/input.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { InformationComponent } from './information/information.component';

const routes: Routes = [
  { path: '', component: InputComponent },
  { path: 'namad/:name', component: InformationComponent },
  { path: 'not-found', component: NotfoundComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
