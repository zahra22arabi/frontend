import {
  Component,
  HostBinding,
  Inject,
  Renderer2,
  OnInit,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ThemeOption } from 'ngx-echarts';
import { DataService } from './data.service';
import { ApiService } from './api.service';

import io from 'socket.io-client';
// const socket = io('http://localhost:3000'); //it is in local machine
const socket = io('http://185.231.181.254:3000');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {
  private isDark = false;
  theme: string | ThemeOption = 'undefined';
  timer: any;
  startTime: any;
  endTime: any;

  url: string = '/api/test';
  items: any = [];

  @HostBinding('class')
  get themeMode() {
    return this.isDark ? 'my-dark-theme' : 'my-light-theme';
  }

  // for components which needs a layout
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private data: DataService,
    private apiService: ApiService,
  ) {}

  ngOnInit() {
    this.renderer.setAttribute(this.document.body, 'class', 'my-light-theme');

    console.log(new Date());
    socket.on('validFlag', (res) => {
      this.apiService.validFlag.next(res);
      console.log(res);
    })
    
  }

  switchMode(isDarkMode: boolean) {
    const hostClass = isDarkMode ? 'my-dark-theme' : 'my-light-theme';
    this.renderer.setAttribute(this.document.body, 'class', hostClass);
    if (isDarkMode) {
      this.theme = 'dark';
    } else {
      this.theme = 'undefined';
    }
    this.data.themeStore.next(this.theme);
  }

}
