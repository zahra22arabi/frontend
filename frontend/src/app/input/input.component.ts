import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SEOService } from '../seo.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  names: string[] = [];
  myControl = new FormControl();
  filteredNames: Observable<string[]>;
  selectedName: string;
  form: FormGroup;
  spinFlag: boolean = false;
  private searchEventSubscription: Subscription;

  constructor(
    private apiService: ApiService,
    private data: DataService,
    private router: Router,
    private seoService: SEOService
  ) {}

  ngOnInit(): void {
    this.seoService.updateTitle("چشم انداز بازار بورس تهران");
    this.seoService.updateDescription("نمایش نمادها و شرکت های فعال در بازار بورس تهران در نمودار سلسله مراتبی بر اساس ارزش بازار و حجم معاملات آنها؛ همچنین نمایش تغییرات قیمت آنها در طول روز");
    this.spinFlag = true;
    this.apiService.getNames().then((listOfNames) => {
      this.names = listOfNames;
      this.filterNames();
      this.spinFlag = false;
    });
    this.searchEventSubscription = this.apiService.validFlag.subscribe(() => {
      if (this.names.length != 0) {
        this.apiService.getNames().then((listOfNames) => {
          this.names = listOfNames;
          this.filterNames();
          this.spinFlag = false;
        });
      }
    });
  }

  private filterNames() {
    this.filteredNames = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value))
    );
  }

  private _filter(value: string) {
    let filterValue = this.names.filter((name) => name.includes(value));
    if (filterValue.length != 0) {
      return filterValue;
    } else {
      return ['موردی یافت نشد'];
    }
  }

  click(event: any) {
    if (event.option.value == 'موردی یافت نشد') {
      console.log('he');
      (<HTMLInputElement>document.getElementById('myInput')).value = '';
    } else {
      this.selectedName = event.option.value;
      this.data.selectedValue = this.selectedName;
      this.router.navigate(['namad', this.selectedName]);
    }
  }
}
