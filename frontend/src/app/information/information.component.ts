import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import {
  IPurchase,
  IPrice,
  ITransaction,
  IStatus,
  IBase,
  IPersale,
  ILegal,
} from '../interfaces/app-ITables';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';
import { entry } from '../interfaces/app.info';
import { SEOService } from '../seo.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
})
export class InformationComponent implements OnInit, OnDestroy {
  apiData: string[][];

  purchaseData: IPurchase[];
  purchaseColumns = ['purchase', 'transaction', 'sale'];
  purchaseHeaders = ['خرید', 'معامله', 'فروش'];

  priceData: IPrice[];
  priceColumns = ['firstPurchase', 'lastTransaction', 'yesterdayPrice'];
  priceHeaders = ['اولین', 'پایانی', 'دیروز'];

  transactionData: ITransaction[];
  transactionColumns = [
    'transactionNumber',
    'transactionVolume',
    'transactionValue',
    'numOfShares'
  ];
  transactionHeaders = ['تعداد معاملات', 'حجم معاملات', 'ارزش معاملات','تعداد سهم'];
  horiTransactionColumns: string[] = [];
  horiTransactionData: any[];

  statusData: IStatus[];
  statusColumns = ['time', 'status', 'market', 'industryGroup'];
  statusHeaders = ['آخرین معامله', 'وضعیت نماد', 'بازار', 'گروه صنعت'];
  horiStatusColumns: string[] = [];
  horiStatusData: any[];
  mydate: any = new Date();

  baseData: IBase[];
  baseColumns = [
    'baseVolume',
    'marketValue',
    'EPS',
    'PE',
    'PEGroup',
  ];
  baseHeaders = [
    'حجم مبنا',
    'ارزش بازار',
    'EPS',
    'P/E',
    'P/Eگروه',
  ];
  horiBaseColumns: string[] = [];
  horiBaseData: any[];

  persaleDataReturned: number[][];
  persaleData: IPersale[];
  persaleColumns = [
    'purchaseNumber',
    'purchaseVolume',
    'purchaseCost',
    'saleCost',
    'saleVolume',
    'saleNumber',
  ];
  persaleHeaders = ['تعداد', 'حجم', 'خرید', 'فروش', 'حجم', 'تعداد'];
  persaleHeaderColumns = ['خرید', 'فروش'];

  legalDataReturned: number[];
  legalDataVolume: ILegal[];
  legalDataNumber: ILegal[];
  legalColumns = ['legal', 'purchase', 'sale'];
  legalHeadersVolume = ['حجم', 'خرید', 'فروش'];
  legalHeadersNumber = ['تعداد', 'خرید', 'فروش'];

  colorTheme: string = '';
  private searchEventSubscription: Subscription;

  itemNameInfo: any = [];
  itemName: string;
  itemWholeName: string;
  itemId: number = null;
  oneEntry = {} as entry;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private data: DataService,
    private seoService: SEOService
  ) {}

  ngOnInit(): void {    
    this.renderName().then((name) => {
      this.apiService
        .findSpecificName(name)
        .then((res) => {
          this.itemNameInfo = res;
          if (res.length != 0) {
            this.itemId = res[0].id;
            this.itemWholeName = res[0].wholeName;
            this.seoService.updateTitle(`مشخصات ${this.itemWholeName}`);
            this.seoService.updateDescription("نمایش اطلاعات حقیقی و حقوقی، خرید و فروش، وضعیت نمادها، نمودار قیمت، نمودار شمعی ");
          }
        })
        .then(() => {
          this.searchEventSubscription = this.apiService.validFlag.subscribe(
            () => {
              console.log(
                'must be updated since the data is ready',
                this.itemId
              );
              this.updateInformation(this.itemId);
            }
          );
        });
    });

    this.data.themeStore.subscribe((val) => {
      this.fixColor(val);
    });
  }

  async renderName() {
    const name = await this.route.snapshot.params['name'];
    this.itemName = name;
    return this.itemName;
  }

  async updateInformation(num: number) {
    console.log('updateInformation');
    this.oneEntry = await this.apiService.getOneInfo(num);

    this.purchaseTable();
    this.priceTable();
    this.transactionTable();
    this.statusTable();
    this.baseTable();
    this.persaleTable();
    this.legalTable();
  }

  fixColor(color: string) {
    this.colorTheme = color;
    if (this.colorTheme == 'undefined') {
      document.documentElement.style.setProperty(
        '--td-color',
        'hsl(0, 0%, 96%)'
      );
      document.documentElement.style.setProperty(
        '--td-color-even',
        'hsl(180, 40%, 96%)'
      );
      document.documentElement.style.setProperty(
        '--th-color-even',
        'hsl(180, 40%, 98%)'
      );
      document.documentElement.style.setProperty(
        '--hover',
        'hsl(180, 40%, 93%)'
      );
      document.documentElement.style.setProperty(
        '--purchase',
        'hsl(240, 100%, 80%)'
      );
      document.documentElement.style.setProperty('--sale', 'hsl(0, 100%, 80%)');
      document.documentElement.style.setProperty(
        '--name',
        'hsl(270, 10%, 80%)'
      );
    } else if (this.colorTheme == 'dark') {
      document.documentElement.style.setProperty(
        '--td-color',
        'hsl(240, 2%, 20%)'
      );
      document.documentElement.style.setProperty(
        '--td-color-even',
        'hsl(240, 1%, 30%)'
      );
      document.documentElement.style.setProperty(
        '--th-color-even',
        'hsl(240, 1%, 35%)'
      );
      document.documentElement.style.setProperty(
        '--hover',
        'hsl(336, 100%, 25%)'
      );
      document.documentElement.style.setProperty(
        '--purchase',
        'hsl(240, 80%, 20%)'
      );
      document.documentElement.style.setProperty('--sale', 'hsl(0, 80%, 20%)');
      document.documentElement.style.setProperty(
        '--name',
        'hsl(270, 10%, 40%)'
      );
    }
  }

  // last transaction
  setTime() {
    console.log(this.oneEntry.lastTransactionTime);
    let hour = +this.oneEntry.lastTransactionTime.slice(0, -4);
    let minute = +this.oneEntry.lastTransactionTime.slice(-4, -2);
    let second = +this.oneEntry.lastTransactionTime.slice(-2);
    this.mydate = new Date();
    this.mydate.setHours(hour);
    this.mydate.setMinutes(minute);
    this.mydate.setSeconds(second);
  }

  purchaseTable() {
    this.purchaseData = [
      {
        purchase: '*',
        transaction:
          this.oneEntry.transaction[this.oneEntry.transaction.length - 1],
        sale: '*',
      },
    ];
  }

  priceTable() {
    this.priceData = [
      {
        firstPurchase:
          this.oneEntry.firstPurchase[this.oneEntry.firstPurchase.length - 1],
        lastTransaction: this.oneEntry.lastTransaction,
        yesterdayPrice: this.oneEntry.yesterdayPrice,
      },
    ];
  }

  transactionTable() {
    this.transactionData = [
      {
        transactionNumber: this.formatNumber(this.oneEntry.transactionNumber),
        transactionVolume: this.formatNumber(
          this.oneEntry.transactionVolume[
            this.oneEntry.transactionVolume.length - 1
          ]
        ),
        transactionValue: this.formatNumber(this.oneEntry.transactionValue),
        numOfShares: this.formatNumber(this.oneEntry.sharesNumber),
      },
    ];
    //['0','1']
    for (let num = 0; num <= this.transactionData.length; num++) {
      this.horiTransactionColumns.splice(num, 1, num.toString());
    }
    this.horiTransactionData = this.transactionColumns.map((x) =>
      this.formatInputRow(
        this.transactionColumns,
        this.transactionHeaders,
        this.transactionData,
        x
      )
    );
  }

  statusTable() {
    this.setTime();
    this.statusData = [
      {
        time: this.mydate.toLocaleTimeString('en-GB'),
        status: 'مجاز',
        market: '*',
        industryGroup: '*',
      },
    ];
    //['0','1']
    for (let num = 0; num <= this.statusData.length; num++) {
      this.horiStatusColumns.splice(num, 1, num.toString());
    }
    this.horiStatusData = this.statusColumns.map((x) =>
      this.formatInputRow(
        this.statusColumns,
        this.statusHeaders,
        this.statusData,
        x
      )
    );
  }

  baseTable() {
    this.baseData = [
      {
        baseVolume: this.formatNumber(this.oneEntry.baseVolume),
        marketValue: this.formatNumber(this.apiService.oneEntry.lastTransaction*this.apiService.oneEntry.sharesNumber),
        EPS: this.oneEntry.EPS,
        PE: (this.oneEntry.lastTransaction/this.oneEntry.EPS).toFixed(2),
        PEGroup: '*',
      },
    ];
    //['0','1']
    for (let num = 0; num <= this.baseData.length; num++) {
      this.horiBaseColumns.splice(num, 1, num.toString());
    }
    this.horiBaseData = this.baseColumns.map((x) =>
      this.formatInputRow(this.baseColumns, this.baseHeaders, this.baseData, x)
    );
  }

  persaleTable() {
    this.persaleData = [];
    for (let ind in this.oneEntry.persaleData) {
      this.persaleData.push({
        saleNumber: Object.values(this.oneEntry.persaleData[ind])[1],
        purchaseNumber: Object.values(this.oneEntry.persaleData[ind])[2],
        purchaseCost: Object.values(this.oneEntry.persaleData[ind])[3],
        saleCost: Object.values(this.oneEntry.persaleData[ind])[4],
        purchaseVolume: Object.values(this.oneEntry.persaleData[ind])[5],
        saleVolume: Object.values(this.oneEntry.persaleData[ind])[6],
      });
    }
  }

  legalTable() {
    console.log(this.oneEntry.legalData);
    this.legalDataVolume = [
      {
        legal: 'حقیقی',
        purchase: this.formatNumber(Object.values(this.oneEntry.legalData)[2]),
        sale: this.formatNumber(Object.values(this.oneEntry.legalData)[6]),
      },
      {
        legal: 'حقوقی',
        purchase: this.formatNumber(Object.values(this.oneEntry.legalData)[3]),
        sale: this.formatNumber(Object.values(this.oneEntry.legalData)[7]),
      },
    ];
    this.legalDataNumber = [
      {
        legal: 'مجموع',
        purchase:
          this.formatNumber(Object.values(this.oneEntry.legalData)[0]) +
          this.formatNumber(Object.values(this.oneEntry.legalData)[1]),
        sale:
          this.formatNumber(Object.values(this.oneEntry.legalData)[4]) +
          this.formatNumber(Object.values(this.oneEntry.legalData)[5]),
      },
      {
        legal: 'حقیقی',
        purchase: this.formatNumber(Object.values(this.oneEntry.legalData)[0]),
        sale: this.formatNumber(Object.values(this.oneEntry.legalData)[4]),
      },
      {
        legal: 'حقوقی',
        purchase: this.formatNumber(Object.values(this.oneEntry.legalData)[1]),
        sale: this.formatNumber(Object.values(this.oneEntry.legalData)[5]),
      },
    ];
  }

  formatInputRow(columns, headers, data, row) {
    const output = {};
    let ind = columns.indexOf(row);
    output[0] = headers[ind];
    output[1] = data[0][row];
    return output;
  }

  formatNumber(num: number) {
    let output: any;
    if (num > 1000000000) {
      output = `${(num / 1000000000).toFixed(3)} B`;
    } else if (num > 1000000) {
      output = `${(num / 1000000).toFixed(3)} M`;
    } else if (num > 0) {
      output = num;
    } else if (num == 0) {
      output = 0;
    }
    return output;
  }

  ngOnDestroy(): void {
    this.searchEventSubscription.unsubscribe();
  }
}
