const mongoose = require("mongoose");

// define the schema of a model in the db
const baseInfoSchema = new mongoose.Schema({
  _id: {
    type: Number, //index of the items
    required: true,
  },
  date: String,
  time: [String],
  dn1: String,
  name: String,
  wholeName: String,
  lastTransactionTime: String,
  firstPurchase: [Number],
  lastTransaction: Number,
  transaction: [Number],
  transactionNumber: Number,
  transactionVolume: [Number],
  transactionValue: Number,
  dayMinIndex: [Number],
  dayMaxIndex: [Number],
  yesterdayPrice: Number,
  EPS: Number,
  baseVolume: Number,
  dn16: Number,
  dn17: Number,
  dn18: Number,
  maxPerimittedPrice: Number,
  minPermittedPrice: Number,
  sharesNumber: Number,
  dn22: Number,
  persaleData: [[Number]],
  legalData: [Number],
  freeFloatPercentage: Number
});

module.exports = mongoose.model("BaseInfo", baseInfoSchema);
