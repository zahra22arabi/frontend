const axios = require("axios");
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const BaseInfo = require("./models/baseInfo");
const socket = require("socket.io");
const cors = require("cors");
const moment = require("moment");
const moment_timezone = require("moment-timezone");
moment.tz.setDefault("Asia/Tehran");
const isHoliday = require("shamsi-holiday");

let ids = [];
const wipeDB = async () => {
  try {
    await db.collection("baseinfos").deleteMany({});
    console.log("wiped out successfully");
    BaseInfo.count((err, count) => {
      console.log(count);
    });
  } catch (err) {
    console.error(err);
  }
};

mongoose.connect("mongodb://mongo:27017/stockMarket", {
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (err) => console.error(err));
// db.once("open", wipeDB);

// set up the server to use json
app.use(express.json());
// set the policy to accept requests
app.use(cors({ origin: "*" }));

// start Listening ... 
const server = app.listen(3000, () => {
  console.log("Server started at port 3000");

  timer = setInterval(() => {
    let date = new Date();
    let day = date.getDay(); //Thursday is 4

    if (!isHoliday(date) && day != 4) {
      let myDate = moment().format("HH:mm");
      let startTime = moment("9:01", "HH:mm").format("HH:mm");
      let endTime = moment("12:30", "HH:mm").format("HH:mm");

      if (myDate == startTime) {
        wipeDB().then(() => getDatas("fill"));
      } else if (myDate > startTime && myDate <= endTime) {
        BaseInfo.count((err, count) => {
          if (count == 0) {
            getDatas("fill");
          } else {
            getDatas("update");
          }
        });
      }
    }
  }, 60000);
});


const io = socket(server);
// handle multi-clients
let allClients = [];
io.sockets.on("connection", (socket) => {
  allClients.push(socket);

  // removing the clients from the list of clients
  socket.on("disconnect", () => {
    let i = allClients.indexOf(socket);
    allClients.splice(i, 1);
  });
});

let validFlag = true;
const sendData = (socket) => {
  validFlag = !validFlag;
  socket.emit("validFlag", validFlag);
  console.log(`data is ${validFlag} && socket id is ${socket.id}`);
};

// path to get the list of companies
app.get("/api/getNames", async (req, res) => {
  try {
    const nameOfItems = await BaseInfo.find({}, { name: 1, wholeName: 1 });
    res.status(200).json(nameOfItems);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// path to get the detailed information for one company
app.get("/api/getOneEntry/:id", async (req, res) => {
  try {
    const response = await BaseInfo.find({ _id: req.params.id }, {});
    res.status(200).json(response);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// create the data structure needed for treemap
let groupsName = {
  200: "استاندارد",
  206: "گام بانک",
  208: "سهام سکوک",
  300: "بازار پایه بورس",
  301: "مشارکت شهرداری",
  303: "بازار اول فرابورس",
  305: "بازار ابزارهای نوین مالی بورس",
  306: "اسناد خزانه",
  307: "تسهیلات مسکن",
  308: 200,
  309: 300,
  311: "اختیارخ",
  312: "اختیارف",
  313: "شرکت هاي کوچک و متوسط فرابورس",
  320: 311,
  321: 312,
  327: "گواهی سیمان",
  380: "صندوق س.",
  400: "بازار اول بورس",
  403: 303,
  404: "بازار پایه فرابورس",
  600: "اختیار فروش ت",
  602: 600,
  701: "بورس کالا",
  706: "دولت",
};
let treemap = [];
let repeated = false;
app.get("/api/getTreemapData", async (req, res) => {
  try {
    treemap = [];

    const response = await BaseInfo.find({}, { dn22: 1 });
    let groups = [];
    for (let i in response) {
      groups.push(response[i].dn22);
    }
    let uniqueGroups = Array.from(new Set(groups));
    uniqueGroups.sort(function (a, b) {
      return a - b;
    });

    treemap.push({
      name: "نقشه بورس",
    });

    for (let groupIndex in uniqueGroups) {
      await createTreemapData(uniqueGroups[groupIndex]);
    }

    console.log("finished");
    res.status(200).json(treemap);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

const retrievedName = (key) => {
  if (typeof groupsName[key] == "number") {
    repeated = true;
    return groupsName[groupsName[key]];
  } else {
    repeated = false;
    return groupsName[key];
  }
};

const createTreemapData = async (keyGroup) => {
  min = Infinity;
  max = -Infinity;
  const name = retrievedName(keyGroup);
  let middleObject;
  if (repeated) {
    try {
      middleObjectarray = await treemap.filter((item) => {
        return item.name == name;
      });
      if (middleObjectarray.length != 0) {
        middleObject = middleObjectarray[0];
      } else {
        middleObject = {
          name: name,
          min: 0,
          max: 0,
          children: [],
        };
      }
    } catch (err) {
      console.log(err);
    }
  } else {
    middleObject = {
      name: name,
      min: 0,
      max: 0,
      children: [],
    };
  }

  try {
    const data = await BaseInfo.find(
      { dn22: keyGroup },
      {
        name: 1,
        wholeName: 1,
        transaction: 1,
        firstPurchase: 1,
        lastTransaction: 1,
        sharesNumber: 1,
        transactionVolume: 1,
        transactionValue: 1,
        freeFloatPercentage: 1,
      }
    );

    for (let ind in data) {
      if (data[ind].firstPurchase[data[ind].firstPurchase.length - 1] != 0) {
        let transactionVolume =
          data[ind].transactionVolume[data[ind].transactionVolume.length - 1];
        let firstPurchase =
          data[ind].firstPurchase[data[ind].firstPurchase.length - 1];
        let transaction =
          data[ind].transaction[data[ind].transaction.length - 1];
        let change = ((transaction - firstPurchase) / firstPurchase) * 100;
        if (change > max) {
          max = change;
        }
        if (change < min) {
          min = change;
        }
        middleObject.children.push({
          name: data[ind].name,
          wholeName: data[ind].wholeName,
          value: [
            transactionVolume,
            data[ind].transactionValue / data[ind].freeFloatPercentage,
            transaction,
            firstPurchase,
            change,
          ],
        });
      }
    }
    if ((min == Infinity || max == -Infinity) && !repeated) {
      if (middleObject.children.length == 0) {
        middleObject.min = 0;
        middleObject.max = 0;
      }
    } else if (min != Infinity && max != -Infinity) {
      middleObject.min = min;
      middleObject.max = max;
    }

    if (!repeated) {
      treemap.push(middleObject);
    }
  } catch (err) {
    console.log(err);
  }
};

let columnNames = [
  "index", //0
  "dn1", //1
  "name", //2
  "wholeName", //3
  "lastTransactionTime", //4
  "firstPurchase", //5
  "lastTransaction", //6
  "transaction", //7
  "transactionNumber", //8
  "transactionVolume", //9
  "transactionValue", //10
  "dayMinIndex", //11
  "dayMaxIndex", //12
  "yesterdayPrice", //13
  "EPS", //14
  "baseVolume: Number", //15
  "dn16", //16
  "dn17", //17
  "dn18", //18
  "maxPerimittedPrice", //19
  "minPermittedPrice", //20
  "sharesNumber", //21
  "dn22", //22-Company
];

// async function getDatas
const getDatas = async (mode, req = null, res = null) => {
  try {
    const response = await axios({
      url: "http://www.tsetmc.com/tsev2/data/MarketWatchInit.aspx?h=0&r=0",
      method: "get",
    });

    console.log("start of getting data", mode);
    await organize_data(response.data, mode);
    console.log("entering the legal data");

    try {
      const response_haghighi = await axios({
        url: "http://www.tsetmc.com//tsev2/data/ClientTypeAll.aspx",
        method: "get",
      });
      await organize_haghighi_data(response_haghighi.data);
    } catch (err) {
      console.log(mode in fill);
      if (mode == "fill") {
        try {
          await BaseInfo.updateMany(
            {},
            { $set: { legalData: [0, 0, 0, 0, 0, 0, 0, 0] } }
          );
        } catch (err) {
          console.log(err);
        }
      }
      console.log(err);
    }

    allClients.forEach((item, ind) => sendData(item));
    console.log("finish");

    if (res) {
      res.status(200).json(response_haghighi.data);
    }
  } catch (err) {
    if (res) {
      res.status(500).json({ message: err });
    }
  }
};

const organize_data = async (responseText, mode) => {
  console.log("in organize_data...", mode);
  let listOfItems = [];
  let firstItemOfData = [];
  let lastItemOfData = [];

  for (var item of responseText.split(";")) {
    listOfItems.push(item.split(","));
  }
  if (!listOfItems[0][18].startsWith("@"))
    throw new Error("It doesn't strat with @");
  listOfItems[0][18] = listOfItems[0][18].replace("@", "");

  let dateTime = listOfItems[0][2].split("@")[1];
  let persianDate = dateTime.split(" ")[0];
  let time = dateTime.split(" ")[1];
  time = time.split(":")[0] + ":" + time.split(":")[1];
  // console.log(persianDate , time);

  firstItemOfData = listOfItems[0].slice(18);

  if (!firstItemOfData.length == 23)
    throw new Error("The length of the first data is wrong");

  if (mode == "fill") {
    await fillDB(persianDate, time, firstItemOfData);
  } else if (mode == "update") {
    await updateDB(persianDate, time, firstItemOfData);
  }

  let num_symbols = 0;
  for (let i = 1; i < listOfItems.length; i++) {
    if (listOfItems[i].length != 23) {
      num_symbols = i;
      break;
    }
    if (mode == "fill") {
      await fillDB(persianDate, time, listOfItems[i]);
    } else if (mode == "update") {
      await updateDB(persianDate, time, listOfItems[i]);
    }
  }

  if (!(num_symbols > 1 && listOfItems[num_symbols][22].includes("@")))
    throw new Error("It doesn't include with @");
  let dataRelated = listOfItems[num_symbols][22].split("@")[0];
  let persaleRelated = +listOfItems[num_symbols][22].split("@")[1];
  lastItemOfData = listOfItems[num_symbols].slice(0, 22).concat([dataRelated]);
  if (mode == "fill") {
    await fillDB(persianDate, time, lastItemOfData);
    console.log("FILL3");
  } else if (mode == "update") {
    await updateDB(persianDate, time, lastItemOfData);
    console.log("UPDATE3");
  }

  if (mode == "fill") {
    findFreeFloat(ids); //async
  }

  //persale data
  let persaleData = [];
  let temporaryList = listOfItems[num_symbols].slice(23).map((item) => {
    return +item;
  });
  persaleData[0] = [persaleRelated].concat(temporaryList);

  num_symbols += 1;
  for (let i = num_symbols; i < listOfItems.length; i++) {
    if (i == listOfItems.length - 1) {
      if (!listOfItems[i][7].includes("@"))
        throw new Error("It doesn't include with @");
      let lastItem = +listOfItems[i][7].split("@")[0];
      let temporaryList = listOfItems[i].slice(0, 7).map((item) => {
        return +item;
      });
      persaleData.push(temporaryList.concat([lastItem]));
      num_symbols = i;
      break;
    }
    persaleData.push(
      listOfItems[i].map((item) => {
        return +item;
      })
    );
  }

  console.log("end of first era0");
  await addpersaleDataToDB(persaleData, mode);
  console.log("end of first era1");
};

const findFreeFloat = async (ids) => {
  console.log("****************",new Date());
  const re = /KAjCapValCpsIdx='(\d*)'/i;
  let found;
  for (let i = 0; i < ids.length; i++) {
    url = `http://www.tsetmc.com/Loader.aspx?ParTree=151311&i=${ids[i].id}`;
    try {
      const res = await axios({
        url: url,
        method: "get",
      });
      found = +res.data.match(re)[1];
      ids[i].percentage = found;
    } catch (err) {
      console.log("error");
    }
    query = { _id: ids[i].id };
    update = {
      $set: {
        freeFloatPercentage: ids[i].percentage,
      },
    };
    await BaseInfo.findByIdAndUpdate(query, update);
  }
  console.log("************FREE FLOAT FINISHES************",new Date());
};

const fillDB = async (date, time, arrayOneItem) => {
  let freefloat = 0;
  if (ids.length == 0) {
    ids.push({
      id: +arrayOneItem[0],
      percentage: 1,
    });
    freefloat = 1;
  } 
  else if (
    !ids.find(({ id }) => {
      id == +arrayOneItem[0];
    })
  ) {
    ids.push({
      id: +arrayOneItem[0],
      percentage: 1,
    });
    freefloat = 1;
  } else {
    freefloat = ids.filter(({ id }) => {
      return id == +arrayOneItem[0];
    })[0].percentage;
  }

  let sampleData = new BaseInfo({
    _id: +arrayOneItem[0],
    date: date,
    time: [time],
    persaleData: [],
    legalData: [],
    freeFloatPercentage: freefloat,
  });
  //index added above
  for (let i = 1; i < 23; i++) {
    if (
      columnNames[i] == "dn1" ||
      columnNames[i] == "name" ||
      columnNames[i] == "wholeName" ||
      columnNames[i] == "lastTransactionTime"
    ) {
      sampleData[columnNames[i]] = arrayOneItem[i];
    } else {
      sampleData[columnNames[i]] = +arrayOneItem[i];
    }
  }
  await sampleData.save();
};

const updateDB = async (date, time, arrayOneItem) => {
  // console.log("in update funtion", arrayOneItem[0], typeof arrayOneItem[0]);
  query = { _id: +arrayOneItem[0] };
  update = {
    $set: {
      date: date,
      dn1: arrayOneItem[1],
      name: arrayOneItem[2],
      wholeName: arrayOneItem[3],
      lastTransactionTime: arrayOneItem[4],
      lastTransaction: +arrayOneItem[6],
      transactionNumber: +arrayOneItem[8],
      transactionValue: +arrayOneItem[10],
      yesterdayPrice: +arrayOneItem[13],
      EPS: +arrayOneItem[14],
      baseVolume: +arrayOneItem[15],
      maxPerimittedPrice: +arrayOneItem[19],
      minPermittedPrice: +arrayOneItem[20],
      sharesNumber: +arrayOneItem[21],
    },
    $push: {
      time: time,
      firstPurchase: +arrayOneItem[5],
      transaction: +arrayOneItem[7],
      transactionVolume: +arrayOneItem[9],
      dayMinIndex: +arrayOneItem[11],
      dayMaxIndex: +arrayOneItem[12],
    },
  };
  await BaseInfo.findByIdAndUpdate(query, update);
};

const addpersaleDataToDB = async (persaleData, mode) => {
  if (mode == "update") {
    try {
      await BaseInfo.updateMany({}, { $set: { persaleData: [] } });
    } catch (err) {
      console.log(err);
    }
  }

  for (var i = 0; i < persaleData.length; i++) {
    try {
      await BaseInfo.findByIdAndUpdate(
        { _id: persaleData[i][0] },
        { $push: { persaleData: persaleData[i].slice(1) } }
      );
    } catch (err) {
      console.log(err);
    }
  }

  console.log("end of second era");
};

const organize_haghighi_data = async (responseText) => {
  let legalData = [];
  let data = [];
  try {
    if (responseText.length != 0) {
      for (var item of responseText.split(";")) {
        data.push(item.split(","));
      }
      for (let i = 1; i < data.length; i++) {
        legalData.push(
          data[i].map((item) => {
            return +item;
          })
        );
      }

      for (var i = 0; i < legalData.length; i++) {
        await BaseInfo.findByIdAndUpdate(
          { _id: legalData[i][0] },
          { $set: { legalData: legalData[i].slice(1) } }
        );
      }
    }
  } catch (err) {
    console.log(err);
  }
  console.log("end of third era");
};
